#include "SDL2/SDL.h"
#include "ImageViewer.h"

// Constructeur qui initialise tout SDL2 et crée la fenêtre et le renderer
ImageViewer::ImageViewer() {
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow("ImageViewer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 200, 200, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

// Détruit et ferme SDL2
ImageViewer::~ImageViewer() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

// Affiche l’image passée en paramètre
void ImageViewer::afficher(const Image& im){
    // Conversion de l'image en SDL_Surface
    SDL_Surface* surface = SDL_CreateRGBSurfaceFrom((void*)im.data, im.width, im.height, 32, im.width * 4, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);

    // Création de la texture à partir de la surface
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);

    // Affichage de la texture
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);

    // Gestion des événements pour le zoom et la fermeture
    bool running = true;
    SDL_Event event;
    while (running) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = false;
            } else if (event.type == SDL_KEYDOWN) {
                switch (event.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        running = false;
                        break;
                    case SDLK_t:
                        // Code pour zoomer
                        break;
                    case SDLK_g:
                        // Code pour dézoomer
                        break;
                }
            }
        }
    }

    // Nettoyage
    SDL_DestroyTexture(texture);
    SDL_FreeSurface(surface);
}



