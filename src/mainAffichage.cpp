#include "Image.h"
#include "ImageViewer.h"
#include "SDL2/SDL.h"

int main(int argc, char** argv) {
    // Création d'une image de dimensions 61x61 pixels
    Image image(61, 61);

    // Définition des couleurs
    Pixel blanc(255, 255, 255);
    Pixel bleu(0, 0, 255);
    Pixel noir(0, 0, 0);
    Pixel rouge(255, 0, 0);
    Pixel jaune(255, 255, 0);
    Pixel vert(0, 255, 0);

    // Remplissage de l'image avec la couleur blanche
    image.effacer(blanc);

    // Dessin de rectangles colorés sur l'image
    image.dessinerRectangle(3, 22, 18, 37, rouge);
    image.dessinerRectangle(5, 24, 16, 35, blanc);
    image.dessinerRectangle(23, 22, 38, 37, noir);
    image.dessinerRectangle(25, 24, 36, 35, blanc);
    image.dessinerRectangle(43, 22, 58, 37, bleu);
    image.dessinerRectangle(45, 24, 56, 35, blanc);
    image.dessinerRectangle(13, 29, 28, 44, jaune);
    image.dessinerRectangle(15, 31, 26, 42, blanc);
    image.dessinerRectangle(33, 29, 48, 44, vert);
    image.dessinerRectangle(35, 31, 46, 42, blanc);

    // Affichage de l'image à l'aide de l'ImageViewer
    ImageViewer imview;
    imview.afficher(image);

    // Fin du programme
    return 0;
}

