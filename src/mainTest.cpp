#include "Image.h"
#include <iostream>

using namespace std;

int main() {
    // Création d'instances de la classe Pixel
    Pixel p1;             // Utilisation du constructeur par défaut
    Pixel p2(5, 10, 40);   // Utilisation du constructeur avec des valeurs spécifiées

    // Affichage des composantes des pixels
    cout << "Le premier pixel est " << p1.r << " " << p1.g << " " << p1.b << endl;
    cout << "Le deuxième pixel est " << p2.r << " " << p2.g << " " << p2.b << endl;

    // Exécution de la fonction de test de regression de la classe Image
    Image::testRegression();

    // Fin du programme
    return 0;
}

