#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED

#include "Pixel.h"  // Inclusion de la classe Pixel
#include <string>   // Inclusion de la bibliothèque pour manipuler des chaînes de caractères

/**
 * @class Image
 * @brief Représente une image composée de pixels.
 */
class Image {
private:
    unsigned int dimx, dimy;  // Dimensions de l'image
    Pixel *tab;               // Tableau de pixels

public:
    //Pointeur vers les données de l'image et dimensions (à des fins inconnues)
    void* data;
    int width;
    int height;

    // Constructeurs et destructeur
    /**
     * @brief Constructeur par défaut de la classe Image.
     */
    Image();

    /**
     * @brief Constructeur de la classe Image avec des dimensions spécifiées.
     * @param dimensionX La dimension en largeur de l'image.
     * @param dimensionY La dimension en hauteur de l'image.
     */
    Image(unsigned int dimensionX, unsigned int dimensionY);

    /**
     * @brief Destructeur de la classe Image.
     */
    ~Image();

    // Méthodes pour accéder et manipuler les pixels
    /**
     * @brief Accès en lecture à un pixel de l'image.
     * @param x Coordonnée en abscisse du pixel.
     * @param y Coordonnée en ordonnée du pixel.
     * @return Une référence constante vers le pixel à la position (x, y).
     */
    const Pixel& getPix(int x, int y) const;

    /**
     * @brief Accès en lecture/écriture à un pixel de l'image.
     * @param x Coordonnée en abscisse du pixel.
     * @param y Coordonnée en ordonnée du pixel.
     * @return Une référence vers le pixel à la position (x, y).
     */
    Pixel& getPix(int x, int y);

    /**
     * @brief Modification d'un pixel de l'image.
     * @param x Coordonnée en abscisse du pixel.
     * @param y Coordonnée en ordonnée du pixel.
     * @param couleur Nouvelle couleur du pixel.
     */
    void setPix(int x, int y, Pixel couleur);

    /**
     * @brief Dessin d'un rectangle sur l'image.
     * @param Xmin Coordonnée minimale en abscisse du rectangle.
     * @param Ymin Coordonnée minimale en ordonnée du rectangle.
     * @param Xmax Coordonnée maximale en abscisse du rectangle.
     * @param Ymax Coordonnée maximale en ordonnée du rectangle.
     * @param couleur Couleur du rectangle.
     */
    void dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, Pixel couleur);

    /**
     * @brief Remplissage de l'image avec une couleur spécifiée.
     * @param couleur Couleur de remplissage.
     */
    void effacer(Pixel couleur);

    // Méthodes statiques (hors instance)
    /**
     * @brief Méthode de test statique pour la classe Image.
     */
    static void testRegression();

    // Méthodes pour sauvegarder, ouvrir et afficher l'image
    /**
     * @brief Sauvegarde de l'image dans un fichier.
     * @param filename Nom du fichier de sauvegarde.
     */
    void sauver(const std::string &filename) const;

    /**
     * @brief Ouverture d'une image depuis un fichier.
     * @param filename Nom du fichier à ouvrir.
     */
    void ouvrir(const std::string &filename);

    /**
     * @brief Affichage de l'image dans la console.
     */
    void afficherConsole() const;

    // Méthodes d'accès pour les données de l'image
    /**
     * @brief Accès aux données de l'image.
     * @return Un pointeur vers les données de l'image.
     */
    void* getData() const { return data; }

    /**
     * @brief Obtention de la largeur de l'image.
     * @return La largeur de l'image.
     */
    int getWidth() const { return width; }

    /**
     * @brief Obtention de la hauteur de l'image.
     * @return La hauteur de l'image.
     */
    int getHeight() const { return height; }
};

#endif

