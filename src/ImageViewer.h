#ifndef IMAGEVIEWER_H_INCLUDED
#define IMAGEVIEWER_H_INCLUDED

#include "Image.h"      // Inclusion de la classe Image
#include "SDL2/SDL.h"   // Inclusion de la bibliothèque SDL

/**
 * @class ImageViewer
 * @brief Permet d'afficher une image à l'aide de la bibliothèque SDL.
 */
class ImageViewer {
private:
    SDL_Window* window;    // Fenêtre SDL
    SDL_Renderer* renderer; // Rendu SDL

public:
    /**
     * @brief Constructeur de la classe ImageViewer.
     */
    ImageViewer();

    /**
     * @brief Destructeur de la classe ImageViewer.
     */
    ~ImageViewer();

    /**
     * @brief Procédure pour afficher une image.
     * @param im L'image à afficher.
     */
    void afficher(const Image& im);
};

#endif

