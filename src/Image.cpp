#include <iostream>
#include <cassert>
#include <fstream>  // Ajout pour la gestion de fichiers
#include "Image.h"

// Constructeur par défaut
Image::Image() : dimx(0), dimy(0), tab(nullptr) {}

// Constructeur paramétré
Image::Image(unsigned int dimensionX, unsigned int dimensionY) : dimx(dimensionX), dimy(dimensionY) {
    tab = new Pixel[dimx * dimy];
}

// Destructeur
Image::~Image() {
    if (tab != nullptr) {
        delete[] tab;
    }
    dimx = dimy = 0;
}

// Obtenir le pixel (lecture seule)
const Pixel& Image::getPix(int x, int y) const {
    static Pixel defaultPixel;  // Objet statique
    if (x < 0 || x >= dimx || y < 0 || y >= dimy) {
        return defaultPixel;
    }
    return tab[y * dimx + x];
}




// Obtenir le pixel (modifiable)
Pixel& Image::getPix(int x, int y) {
    if (x < 0 || x >= dimx || y < 0 || y >= dimy) {
        static Pixel blackPixel;
        return blackPixel;
    }
    return tab[y * dimx + x];
}

// Définir la couleur du pixel
void Image::setPix(int x, int y, Pixel couleur) {
    if (x >= 0 && x < dimx && y >= 0 && y < dimy) {
        tab[y * dimx + x] = couleur;
    }
}

// Dessiner un rectangle
void Image::dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, Pixel couleur) {
    for (int x = Xmin; x <= Xmax; ++x) {
        for (int y = Ymin; y <= Ymax; ++y) {
            setPix(x, y, couleur);
        }
    }
}

void Image::effacer(Pixel couleur) {
    dessinerRectangle(0, 0, dimx - 1, dimy - 1, couleur);
}

// Fonction de test de régression
void Image::testRegression() {
    // Test du constructeur par défaut
    Image image;
    assert(image.dimx == 0);
    assert(image.dimy == 0);
    assert(image.tab == nullptr);

    // Test du constructeur avec copie
    Image image2(5, 5);
    assert(image2.dimx == 5);
    assert(image2.dimy == 5);
    assert(image2.tab != nullptr);
    for (unsigned int x = 0; x < image2.dimx; x++) {
        for (unsigned int y = 0; y < image2.dimy; y++) {
            assert(image2.tab[y * image2.dimx + x].r == 0);
            assert(image2.tab[y * image2.dimx + x].g == 0);
            assert(image2.tab[y * image2.dimx + x].b == 0);
        }
    }

    // Test de set et get
    Pixel pixel(250, 128, 114);
    image2.setPix(3, 3, pixel);

    Pixel p = image2.getPix(3, 3);
    assert(p.r == 250);
    assert(p.g == 128);
    assert(p.b == 114);

    // Test de dessinerRectangle
    Pixel pixel2(255, 255, 0);
    image2.dessinerRectangle(2, 2, 3, 3, pixel2);
    for (unsigned int x = 2; x <= 3; x++) {
        for (unsigned int y = 2; y <= 3; y++) {
            assert(image2.tab[y * image2.dimx + x].r == 255);
            assert(image2.tab[y * image2.dimx + x].g == 255);
            assert(image2.tab[y * image2.dimx + x].b == 0);
        }
    }

    // Test de effacer
    image2.effacer(Pixel(0, 0, 0)); // Effacer l'image en la remplissant de noir
    for (unsigned int x = 0; x < image2.dimx; x++) {
        for (unsigned int y = 0; y < image2.dimy; y++) {
            assert(image2.tab[y * image2.dimx + x].r == 0);
            assert(image2.tab[y * image2.dimx + x].g == 0);
            assert(image2.tab[y * image2.dimx + x].b == 0);
        }
    }

    std::cout << "Tests de Regression réussis!" << std::endl;
}

// Sauvegarder l'image dans un fichier
void Image::sauver(const std::string &filename) const {
    std::ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << std::endl;
    fichier << dimx << " " << dimy << std::endl;
    fichier << "255" << std::endl;
    for (unsigned int y = 0; y < dimy; ++y) {
        for (unsigned int x = 0; x < dimx; ++x) {
            const Pixel pix = getPix(x, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
    }
    std::cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

// Ouvrir une image depuis un fichier
void Image::ouvrir(const std::string &filename) {
    std::ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    char r, g, b;
    std::string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];
    for (unsigned int y = 0; y < dimy; ++y) {
        for (unsigned int x = 0; x < dimx; ++x) {
            fichier >> r >> b >> g;
            getPix(x, y).r = r;
            getPix(x, y).g = g;
            getPix(x, y).b = b;
        }
    }
    fichier.close();
    std::cout << "Lecture de l'image " << filename << " ... OK\n";
}

// Afficher l'image dans la console
void Image::afficherConsole() const {
    std::cout << dimx << " " << dimy << std::endl;
    for (unsigned int y = 0; y < dimy; ++y) {
        for (unsigned int x = 0; x < dimx; ++x) {
            const Pixel &pix = getPix(x, y);
            std::cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        std::cout << std::endl;
    }
}


