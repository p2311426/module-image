#ifndef _PIXEL_H
#define _PIXEL_H

/**
 * @struct Pixel
 * @brief Représente un pixel composé de composantes rouge, verte et bleue.
 */
struct Pixel {
    unsigned char r;  // Composante rouge
    unsigned char g;  // Composante verte
    unsigned char b;  // Composante bleue

    /**
     * @brief Constructeur par défaut de la structure Pixel.
     * Initialise les composantes rouge, verte et bleue à 0.
     */
    Pixel() {
        r = g = b = 0;
    }

    /**
     * @brief Constructeur de la structure Pixel avec initialisation des composantes.
     * @param nr Valeur de la composante rouge.
     * @param ng Valeur de la composante verte.
     * @param nb Valeur de la composante bleue.
     */
    Pixel(unsigned char nr, unsigned char ng, unsigned char nb) {
        r = nr;
        g = ng;
        b = nb;
    }
};

#endif

