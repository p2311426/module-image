#include "Image.h"

int main() {
    // Définition des couleurs
    Pixel rouge(205, 9, 13);
    Pixel jaune(242, 248, 22);
    Pixel bleu(120, 193, 246);

    // Création d'une image de dimensions 64x48 pixels
    Image image1(64, 48);

    // Remplissage de l'image avec la couleur bleue
    image1.effacer(bleu);

    // Dessin d'un rectangle rouge dans l'image1
    image1.dessinerRectangle(6, 8, 26, 21, rouge);

    // Modification de certains pixels avec la couleur jaune
    image1.setPix(16, 14, jaune);
    image1.setPix(46, 33, jaune);

    // Sauvegarde de l'image1 dans un fichier PPM
    image1.sauver("./data/image1.ppm");

    // Création d'une nouvelle image (image2)
    Image image2;

    // Chargement de l'image1 depuis le fichier PPM
    image2.ouvrir("./data/image1.ppm");

    // Dessin de rectangles rouges et jaunes dans l'image2
    image2.dessinerRectangle(23, 18, 37, 28, rouge);
    image2.dessinerRectangle(34, 26, 50, 36, jaune);

    // Sauvegarde de l'image2 dans un fichier PPM
    image2.sauver("./data/image2.ppm");

    // Fin du programme
    return 0;
}

