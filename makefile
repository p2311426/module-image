# La cible 'all' compile les exécutables test et exemple
all :./bin/test ./bin/exemple ./bin/Affichage

# Compilation de l'exécutable 'test' 
./bin/test: ./obj/mainTest.o ./obj/Image.o
	g++ ./obj/mainTest.o ./obj/Image.o -o ./bin/test

# Compilation de l'exécutable 'exemple'
./bin/exemple: ./obj/mainExemple.o ./obj/Image.o
	g++ ./obj/mainExemple.o ./src/Pixel.h ./obj/Image.o -o ./bin/exemple

# Compilation de l'exécutable 'Affichage' 	
./bin/Affichage: ./obj/mainAffichage.o ./obj/ImageViewer.o ./obj/Image.o
	g++ ./obj/mainAffichage.o ./obj/ImageViewer.o ./obj/Image.o -o ./bin/Affichage -lSDL2
		
# Règles de compilation des fichiers objets		
./obj/mainExemple.o: ./src/Image.h ./src/mainExemple.cpp
	g++ -g -c ./src/mainExemple.cpp -o ./obj/mainExemple.o

./obj/mainAffichage.o:./src/ImageViewer.h ./src/mainAffichage.cpp
	g++ -g -c ./src/mainAffichage.cpp -o ./obj/mainAffichage.o
    
./obj/mainTest.o: ./src/Image.h ./src/Pixel.h ./src/mainTest.cpp
	g++ -g -c ./src/mainTest.cpp -o ./obj/mainTest.o
    
./obj/Image.o: ./src/Image.h ./src/Image.cpp ./src/Pixel.h
	g++ -g -c ./src/Image.cpp -o obj/Image.o

./obj/ImageViewer.o:./src/ImageViewer.h ./src/ImageViewer.cpp ./src/Image.h
	g++ -g -c ./src/ImageViewer.cpp -o obj/ImageViewer.o
    
# Génération de la documentation à partir du fichier de configuration doxyfile    
doc: doc/doxyfile
	doxygen doc/doxyfile
    
# Nettoyage des fichiers objets, exécutables et données
clean:
	rm -f ./obj/* ./bin/* ./data/*

